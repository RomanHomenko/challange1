//
//  DetailViewController.swift
//  Challenge1
//
//  Created by Роман Хоменко on 28.03.2022.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet var flagNameLabel: UILabel!
    @IBOutlet var flagImage: UIImageView!
    
    var selectedImage: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .systemGray5
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareFlag))

        if let imageToLoad = selectedImage {
            flagImage.image = UIImage(named: imageToLoad)
            flagNameLabel.text = imageToLoad.components(separatedBy: "@").first
        }
    }
    
    @objc func shareFlag() {
        guard let image = flagImage.image?.jpegData(compressionQuality: 0.8)  else {
            print("no images found")
            return
        }
        
        let activityVC = UIActivityViewController(activityItems: [image], applicationActivities: [])
        activityVC.popoverPresentationController?.barButtonItem = navigationItem.rightBarButtonItem
        activityVC.excludedActivityTypes = [UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook]
        present(activityVC, animated: true)
    }
}
